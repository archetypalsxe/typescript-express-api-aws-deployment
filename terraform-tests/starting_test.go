package test

import (
	"fmt"
	http_helper "github.com/gruntwork-io/terratest/modules/http-helper"
	terraform "github.com/gruntwork-io/terratest/modules/terraform"
	assert "github.com/stretchr/testify/assert"
	"os"
	"os/exec"
	"testing"
	"time"
)

func DeployEcr(project_name string) {
	currentDir, error := os.Getwd()
	os.Chdir("../Express")

	result, error := exec.Command("sh", "scripts/deploy-docker.sh", project_name).Output()
	if error != nil {
		fmt.Printf("\n******Error: %s\n", error)
		fmt.Printf("\nOutput: %s\n\n\n", string(result))
		panic(error)
	}

	fmt.Printf("\nResult: %s\n\n\n", string(result))

	os.Chdir(currentDir)
}

func TestTerraformBasicExample(t *testing.T) {
	t.Parallel()

	var project_name string = "typescript-express-test"

	// The state has to exist before we can do anything further
	stateTerraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		TerraformDir: "../Terraform",

		Vars: map[string]interface{}{
			"project_name":             project_name,
			"force_destroy_s3_buckets": true,
		},

		// Secondary terraform.tfvars in Terraform directory
		VarFiles: []string{"../terraform-tests/terraform-state.tfvars"},

		NoColor:     true,
		Reconfigure: true,
		BackendConfig: map[string]interface{}{
			"path": "terraform-testing.tfstate",
		},

		// https://github.com/gruntwork-io/terratest/issues/913#issuecomment-849499776
		EnvVars: map[string]string{
			"TF_DATA_DIR": ".terraform-" + project_name,
		},
	})

	defer terraform.Destroy(t, stateTerraformOptions)
	terraform.InitAndApply(t, stateTerraformOptions)

	DeployEcr(project_name)

	mainTerraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		TerraformDir: "../Terraform/main",

		// Variables to pass to our Terraform code using -var options
		Vars: map[string]interface{}{
			"project_name":             project_name,
			"force_destroy_s3_buckets": true,
			// Currently cannot override variables in the tfvars file due to the order they are processed
			// https://github.com/gruntwork-io/terratest/pull/256
			// "deploy_elastic_beanstalk": true,
			// "deploy_fargate":           true,
		},

		// Variables to pass to our Terraform code using -var-file options
		VarFiles: []string{"../../terraform-tests/terraform-main.tfvars"},

		// Disable colors in Terraform commands so its easier to parse stdout/stderr
		NoColor:     true,
		Reconfigure: true,
		// https://github.com/gruntwork-io/terratest/blob/master/test/terraform_backend_example_test.go
		BackendConfig: map[string]interface{}{
			"bucket":         "terraform-state-bucket-" + project_name,
			"key":            "tfstate-test/terraform.tfstate",
			"region":         "us-east-1",
			"dynamodb_table": "terraform-lock-table-" + project_name,
		},
		EnvVars: map[string]string{
			"TF_DATA_DIR": ".terraform-" + project_name,
		},
	})

	defer terraform.Destroy(t, mainTerraformOptions)
	terraform.InitAndApply(t, mainTerraformOptions)

	beanStalkUrl := terraform.Output(t, mainTerraformOptions, "beanstalk_url")
	fargateUrl := terraform.Output(t, mainTerraformOptions, "fargate_url")
	beanStalkHealthUrl := terraform.Output(t, mainTerraformOptions, "beanstalk_health_url")
	fargateHealthUrl := terraform.Output(t, mainTerraformOptions, "fargate_health_url")

	for _, url := range [4]string{beanStalkUrl, fargateUrl, beanStalkHealthUrl, fargateHealthUrl} {
		assert.NotEqual(t, "Not deployed", url, "We should get a valid URL")
	}

	//  HttpGetWithRetry(t testing.TestingT, url string, tlsConfig *tls.Config, expectedStatus int, expectedBody string, retries int, sleepBetweenRetries time.Duration)
	http_helper.HttpGetWithRetry(t, beanStalkHealthUrl, nil, 200, "{\"status\":\"pass\"}", 30, 5*time.Second)
	http_helper.HttpGetWithRetry(t, fargateHealthUrl, nil, 200, "{\"status\":\"pass\"}", 30, 5*time.Second)
}
