#!/bin/bash

# Exit if any of the commands fail along the way
set -e

sh scripts/deploy-docker.sh

sh scripts/beanstalk-commands.sh