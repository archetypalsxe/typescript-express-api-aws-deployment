#!/bin/bash

# Exit if any of the commands fail along the way
set -e

aws ecs update-service --cluster typescript-express --service typescript-express --force-new-deployment