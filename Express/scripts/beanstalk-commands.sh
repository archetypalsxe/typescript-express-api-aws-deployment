#!/bin/bash

# Exit if any of the commands fail along the way
set -e

eb deploy typescript-express-test -l typescript-express-application-version
eb open