#!/bin/bash

# Exit if any of the commands fail along the way
set -e

if [ -z "$1" ]; then
  ecr_repo="typescript-express"
else
  ecr_repo=$1
fi

ecr_uri=$(aws ecr describe-repositories | jq --arg ecr_repo "$ecr_repo" -rc '.repositories[] | select(.repositoryName | test($ecr_repo)).repositoryUri')

if [ -z "$ecr_uri" ]; then
  echo "ECR repository ($ecr_repo) was not found"
  exit 1
fi

echo "ECR Repository URI: $ecr_uri"

latest_tag=$ecr_uri:latest
git_hash=$(git log -n 1 --pretty=format:%H)
git_tag=$ecr_uri:$git_hash

if (! docker stats --no-stream ); then
  echo "Docker daemon is not running"
  exit 1
fi

docker build . -t $latest_tag -t $git_tag

aws ecr get-login-password | docker login --username AWS --password-stdin $ecr_uri
docker push $latest_tag
docker push $git_tag