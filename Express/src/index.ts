import express from 'express';
import http from 'http';
import { ApolloServer } from 'apollo-server-express';
import { ApolloServerPluginDrainHttpServer } from 'apollo-server-core';
import resolvers from './graphql/resolvers';
import typeDefs from './graphql/types';
import CountrySource from './graphql/dataSources/country';

const PORT = 3000;

const app = express();
const httpServer = http.createServer(app);
const server = new ApolloServer({
    typeDefs,
    resolvers,
    context: () => ({}),
    introspection: true,
    dataSources: (): any => {
        return {
            countrySource: new CountrySource(),
        };
    },
    csrfPrevention: true,
    plugins: [ApolloServerPluginDrainHttpServer({ httpServer })],
});

server.start().then(res => {
    server.applyMiddleware({ app });
    httpServer.listen({ port: PORT }, (): void =>
    console.log(`🚀 Server ready at http://localhost:${PORT}${server.graphqlPath}`)
    );
});