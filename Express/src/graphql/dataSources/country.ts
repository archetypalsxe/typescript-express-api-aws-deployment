import Database from '../../database';

export default class CountrySource  {
    private _db: any;
    
    async getDatabase() {
        if (!this._db) {
            this._db = new Database();
            await this._db.connect();
        }

        return this._db;
    }

    async convertElements(elements: [Object]) {
        return elements.map((element: any) => {
            return {
                name: element['name'].S,
                population: element['population'].N,
                area: element['area'].N,
                threatenedSpecies: element['threatenedSpecies'].N,
                renewableElectricityProductionPercent: element['renewableElectricityProductionPercent'].N,
                energyUseIntensity: element['energyUseIntensity'].N,
                municipalWaste: element['municipalWaste'].N,
                recycledMunicipalWastePercent: element['recycledMunicipalWastePercent'].N,
                gdpPerCapita: element['gdpPerCapita'].N,
            }
        });
    }

    async getAll(name: String) {
        const db = await this.getDatabase();
        const countries = await db.scan({
            TableName: 'countries',
        });
        if (countries && countries['Items']) {
            return await this.convertElements(countries['Items']);
        }
        return null;
    }

    async get(name: String) {
        const db = await this.getDatabase();
        const country = await db.getItem({
            TableName: 'countries',
            Key: {
                name: {
                    S: name.toString(),
                },
            },
        });
        if (country && country['Item']) {
            return await (await this.convertElements([country['Item']])).pop();
        }
        return null;
    }

    async put(data: any) {
        const parameters = data.parameters || [];

        // https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/DynamoDB/DocumentClient.html
        const country = {
            name: {
                S: data.name.toString(),
            },
            population: {
                N: data.population.toString(),
            },
            area: {
                N: data.area.toString(),
            },
            threatenedSpecies: {
                N: data.threatenedSpecies.toString(),
            },
            renewableElectricityProductionPercent: {
                N: data.renewableElectricityProductionPercent.toString(),
            },
            energyUseIntensity: {
                N: data.energyUseIntensity.toString(),
            },
            municipalWaste: {
                N: data.municipalWaste.toString(),
            },
            recycledMunicipalWastePercent: {
                N: data.recycledMunicipalWastePercent.toString(),
            },
            gdpPerCapita: {
                N: data.gdpPerCapita.toString()
            }
        }

        console.dir(country);

        const db = await this.getDatabase();
        return await db.putItem({
            TableName: 'countries',
            Item: country
        });
    }
}