// https://www.graphql-tools.com/docs/migration/migration-from-merge-graphql-schemas

import "graphql-import-node";
import { mergeTypeDefs } from '@graphql-tools/merge';

import * as util from '../../../graphql/util.graphql';
import countries from '../../../graphql/countries.graphql';

export default mergeTypeDefs([util, countries]);