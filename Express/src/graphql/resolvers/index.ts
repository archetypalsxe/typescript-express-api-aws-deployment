import { mergeResolvers } from '@graphql-tools/merge';
import countryResolver from './countries';

const resolvers = [countryResolver];

export default mergeResolvers(resolvers);