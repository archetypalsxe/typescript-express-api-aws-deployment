# TypeScript Express API AWS Deployment

Started this repository to experiment with deploying a TypeScript Express API to AWS using Terraform. Got everything setup and working very well where it deploys to Elastic Beanstalk and/or Fargate, and wanted to continue experimenting with it. Testing the infrastructure is done with Terratest

## Running Locally
### Docker-Compose
* From `FirstProject` directory
  * `docker-compose build`
  * `docker-compose up`

### Docker
* From the `Express` Directory
* Building
    * `docker build . -t express-ts`
* Running
    * Foreground
        * `docker run -p 3000:3000 express-ts:latest`
    * Background
        * `docker run -p 3000:3000 -d express-ts:latest`
* Accessing
  * `localhost:3000/graphql`

## Terraform

### Setup / Creation

#### Generate the State Bucket and DynamoDB Table
* From the `Terraform` directory
  * `terraform init`
  * `terraform plan`
  * `terraform apply`
* **The ECR repository is created as part of this, before proceeding... an image should be pushed to the repository for other resources to not fail (`scripts/deploy-docker.sh` from the `Express` directory)**

#### All Other Terraform
After the state bucket and DynamoDB lock table have been created, all Terraform commands should be ran from the `Terraform/main` directory
* `terraform init`
* `terraform plan`
* `terraform apply`

If you want to persist variables and not have to type them everytime you run a Terraform command, this can be done through the use of a `terraform.tfvars` file. The file should be in the `Terraform/main` directory. Here is an example of what the file could look like:

```
aws_profile="personal"
region="us-east-1"
deploy_elastic_beanstalk=false
deploy_fargate=true
fargate_domain_name="sub.somewhere.com"
fargate_parent_domain_name="somewhere.com"
beanstalk_domain_name="beanstalk.somewhere.com"
beanstalk_parent_domain_name="somewhere.com"
```

### Testing Terraform
Requires that the Go language be installed

### Pre-requirements
* Copy `Terraform/main/terraform.tfvars` to `terraform-tests/terraform-main.tfvars`
  * This is used instead of the usual Terraform vars file as we [aren't able to override the values](https://github.com/gruntwork-io/terratest/pull/256)
  * Should overwrite:
    * deploy_{elastic_beanstalk and/or fargate}
      * They should both be true
    * `fargate_domain_name`
    * `beanstalk_domain_name`
* Also want to copy `Terraform/terraform.tfvars` file to `terraform-tests/terraform-state.tfvars`
  * Should modify:
    * `project_name`
      * Prevents collisions, I usually just add `-test` to the end
    * `force_destroy_s3_buckets`
      * You want this to be true so that we're able to remove S3 buckets after creating them in the test
    * `force_destroy_ecr_repository`
      * This should also be true so that the ECR repository can be deleted after running tests

### Running
* From the `terraform-tests` directory:
  * `go test -v -timeout 30m`
  * Or: `./runTests.sh`

## Manually Deploying/Updating

### Script to Manually Deploy
* This will perform all the steps in the following sub-sections
  * `Manually Pushing Docker Image to ECR`
  * `Manually Deploying to Beanstalk`
    * **Make sure you have the Elastic Beanstalk environment initialized**
* Running (from the `Express` directory):
  * Deploying to both Fargate and Beanstalk: `./scripts/deploy-both.sh`
  * Deploying to Fargate: `./scripts/deploy-fargate.sh`
  * Deploying to Beanstalk: `./scripts/deploy-beanstalk.sh`

### Manually Pushing Docker Image to ECR
* From the `Express` directory
* Set a variable to make the remaining steps easier:
  * `ecr_uri=$(aws ecr describe-repositories | jq -rc '.repositories[] | select(.repositoryName | test("typescript-express")).repositoryUri')`
  * `docker_tag=$ecr_uri:latest`
    * Creates a `docker_tag` variable that is set to the ECR repository URI along with a tag of latest
    * Assumes that the repository is called `typescript-express`
* Build the image:
  * `docker build . -t $docker_tag`
* Log into ECR:
  * `$(aws ecr get-login --no-include-email)`
* Push up the image:
  * `docker push $docker_tag`
* Probably want to add at least one more tag so that it doesn't get overwritten/untagged
  * `git_hash=$(git log -n 1 --pretty=format:%H)`
  * `docker tag $docker_tag $ecr_uri:$git_hash`
  * `docker push $ecr_uri:$git_hash`

### Manually Deploying to Beanstalk
* If you haven't already initialized (from the `Express` directory:
  * `eb init --profile "${aws_profile:?}" typescript-express`
* `eb deploy typescript-express-test -l typescript-express-application-version`
* Test It
    * `eb open` can be used to open up a browser tab with the appropriate URL

## Fargate

### Running the `exec` Command
The `exec` command can be used to run commands on the running docker containers.

Example scripts:
* Check to make sure that the execute command agent is up and running:
  * `aws ecs describe-tasks --cluster typescript-express --tasks ${taskId:?}`
* Run the execute command to get a bash shell:
  * `aws ecs execute-command --cluster typescript-express --container typescript-express-container-definition --task ${taskId:?} --interactive --command "/bin/bash"`

## Apollo / GraphQL

Example query to add a country:
```
mutation {
    addCountry(
        data: {
          name: "United States",
          population: 321774,
          area: 9833517,
          threatenedSpecies: 1514,
          renewableElectricityProductionPercent: 0,
          energyUseIntensity: 134,
          municipalWaste: 227604,
          recycledMunicipalWastePercent: 26,
          gdpPerCapita: 54306
        }
    ) {
        error
    }
}
```
And another:
```
mutation {
    addCountry(
        data: {
          name: "India",
          population: 1311051,
          area: 3287263,
          threatenedSpecies: 1052,
          renewableElectricityProductionPercent: 0,
          energyUseIntensity: 128,
          municipalWaste: 17569,
          recycledMunicipalWastePercent: 0,
          gdpPerCapita: 1586
        }
    ) {
        error
    }
}
```
Getting entire table:
```
query {
  countries {
    name
    population
    area
    threatenedSpecies
    renewableElectricityProductionPercent
    energyUseIntensity
    municipalWaste
    recycledMunicipalWastePercent
    gdpPerCapita
  }
}
```
Getting specific entry:
```
query {
  country(name: "India") {
    name
    population
    area
    threatenedSpecies
    renewableElectricityProductionPercent
    energyUseIntensity
    municipalWaste
    recycledMunicipalWastePercent
    gdpPerCapita
  }
}
```

### DynamoDB

#### Testing Local Setup
* `aws dynamodb list-tables --endpoint-url http://localhost:8000`

## Acknowledgements
Based on:
* [This article](https://javascript.plainenglish.io/create-and-dockerize-an-express-typescript-application-5c9f6d67ec2f)
* [My previous attempt at a MEAN stack](https://gitlab.com/archetypalsxe/plus-minus-server)
* [Comparison of Docker Hosting on AWS](https://cloudonaut.io/three-ways-to-run-docker-on-aws/)
* [Deploying Fargate Using Terraform](http://fedulov.website/2018/02/18/terraform-deploying-containers-on-aws-fargate/)
* [Deploying Fargate Using Terraform (Second)](https://www.chakray.com/creating-fargate-ecs-task-aws-using-terraform/)
* [AWS Docs on running Fargate from private subnet](https://aws.amazon.com/premiumsupport/knowledge-center/ecs-fargate-tasks-private-subnet/)
* [Fargate Networking](https://www.linkedin.com/pulse/aws-vpc-public-private-subnets-internet-gateway-nat-ridham-dogra/)
* Apollo / GraphQL
  * [Article on Apollo/GraphQL and DynamoDB](https://gannochenko.dev/blog/how-to-use-dynamodb-with-apollo-graphql-and-nodejs-serverless/)
  * [Article Using Apollo with Typescript](https://architectak.medium.com/apollo-server-express-graphql-api-using-node-js-with-typescript-e762afaccb8c)
  * [Article using Kentico Tools](https://www.freecodecamp.org/news/how-to-use-graphql-with-apollo-on-your-website-ecb6046e139/)
* [Data for the Database](https://unstats.un.org/unsd/ENVIRONMENT/Questionnaires/country_snapshots.htm)
