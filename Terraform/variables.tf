variable "region" {
  description = "Region of the AWS VPC"
  type        = string
}

variable "aws_profile" {
  description = "The AWS profile we should be using to interact with Terraform"
  type        = string
}

variable "project_name" {
  description = "The name of the project that we are building"
  default     = "typescript-express"
  type        = string
}

variable "force_destroy_s3_buckets" {
  description = "Whether or not S3 buckets should be able to be deleted with files in them by default"
  default     = false
  type        = bool
}

variable "force_destroy_ecr_repository" {
  description = "Whether or not the ECR repository should be able to be deleted even if there are images in them"
  default     = false
  type        = bool
}