resource "aws_iam_role" "fargate_execution_role" {
  name               = "${var.project_name}-fargate-execution-role"
  assume_role_policy = data.aws_iam_policy_document.fargate_execution_role_assume_policy.json
  managed_policy_arns = [
    "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy",
  ]
}

data "aws_iam_policy_document" "fargate_execution_role_assume_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type = "Service"
      identifiers = [
        "ecs-tasks.amazonaws.com",
      ]
    }
  }
}


resource "aws_iam_role" "fargate_task_role" {
  name               = "${var.project_name}-fargate-task-role"
  assume_role_policy = data.aws_iam_policy_document.fargate_task_role_assume_policy.json
}

data "aws_iam_policy_document" "fargate_task_role_assume_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type = "Service"
      identifiers = [
        "ecs-tasks.amazonaws.com",
      ]
    }
  }
}

data "aws_iam_policy_document" "task_policy" {
  statement {
    sid = "execCommand"
    actions = [
      "ssmmessages:CreateControlChannel",
      "ssmmessages:CreateDataChannel",
      "ssmmessages:OpenControlChannel",
      "ssmmessages:OpenDataChannel",
    ]

    resources = [
      "*",
    ]
  }
}

resource "aws_iam_policy" "task_policy" {
  name   = "${var.project_name}-fargate-task-policy"
  policy = data.aws_iam_policy_document.task_policy.json
}

resource "aws_iam_role_policy_attachment" "task_policy_s3" {
  role       = aws_iam_role.fargate_task_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

resource "aws_iam_role_policy_attachment" "task_policy_dynamodb" {
  role       = aws_iam_role.fargate_task_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
}

resource "aws_iam_role_policy_attachment" "task_policy_attachment" {
  role       = aws_iam_role.fargate_task_role.name
  policy_arn = aws_iam_policy.task_policy.arn
}