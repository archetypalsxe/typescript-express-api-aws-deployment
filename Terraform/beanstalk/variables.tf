variable "region" {
  description = "Region of the AWS VPC"
  type        = string
}

variable "project_name" {
  description = "The name of the project that we are building"
  type        = string
}

variable "ecr_repository" {
  description = "The ECR repository that is used to store Docker images"
}

variable "ecr_tag" {
  description = "Which tag we should be looking for within ECR to use"
  type        = string
}

variable "domain_name" {
  description = "The domain name to create an SSL certificate for"
  type        = string
}

variable "parent_domain_name" {
  description = "If the domain_name is a sub domain, what the parent domain is"
  type        = string
}

variable "force_destroy_s3_buckets" {
  description = "Whether or not S3 buckets should be able to be deleted with files in them by default"
  default     = false
  type        = bool
}