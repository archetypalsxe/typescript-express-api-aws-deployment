# https://medium.com/beck-et-al/elastic-beanstalk-made-simple-end-to-end-automation-with-cloudformation-f3f976309e56
# https://www.flypenguin.de/2017/04/13/elastic-beanstalk-with-docker-using-terraform/

# https://stackoverflow.com/questions/50806263/elastic-beanstalk-instance-profile-not-automatically-created-when-using-terrafor
resource "aws_elastic_beanstalk_environment" "test" {
  name                = "${var.project_name}-test"
  description         = "Test environment for the ${var.project_name}"
  application         = aws_elastic_beanstalk_application.beanstalk_app.name
  solution_stack_name = "64bit Amazon Linux 2 v3.4.14 running Docker"
  version_label       = aws_elastic_beanstalk_application_version.default.name
  tags                = {}
  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "IamInstanceProfile"
    value     = aws_iam_instance_profile.beanstalk_instance_profile.name
  }
  # https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/command-options-general.html
  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "SecurityGroups"
    value     = aws_security_group.ec2_instances.id
  }
  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "RootVolumeSize"
    value     = 50
  }
  setting {
    namespace = "aws:elasticbeanstalk:command"
    name      = "DeploymentPolicy"
    value     = "RollingWithAdditionalBatch"
  }
  setting {
    namespace = "aws:elb:loadbalancer"
    name      = "SecurityGroups"
    value     = aws_security_group.elb.id
  }
  setting {
    namespace = "aws:ec2:vpc"
    name      = "VPCId"
    value     = aws_vpc.vpc.id
  }
  setting {
    namespace = "aws:ec2:vpc"
    name      = "Subnets"
    value     = "${aws_subnet.public_subnet_a.id},${aws_subnet.public_subnet_b.id}"
  }
  setting {
    namespace = "aws:ec2:vpc"
    name      = "AssociatePublicIpAddress"
    value     = true
  }
  setting {
    namespace = "aws:ec2:instances"
    name      = "InstanceTypes"
    value     = "t3.medium"
  }
  setting {
    namespace = "aws:elasticbeanstalk:environment"
    name      = "LoadBalancerType"
    value     = "application"
  }
  setting {
    namespace = "aws:elbv2:listener:default"
    name      = "ListenerEnabled"
    value     = false
  }
  setting {
    namespace = "aws:elbv2:listener:443"
    name      = "Protocol"
    value     = "HTTPS"
  }
  setting {
    namespace = "aws:elbv2:listener:443"
    name      = "SSLCertificateArns"
    value     = aws_acm_certificate.ssl_cert.arn
  }
  setting {
    namespace = "aws:elasticbeanstalk:application"
    name      = "Application Healthcheck URL"
    value     = "/.well-known/apollo/server-health"
  }
  setting {
    namespace = "aws:elasticbeanstalk:environment:process:default"
    name      = "HealthCheckPath"
    value     = "/.well-known/apollo/server-health"
  }
  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "REGION"
    value     = var.region
  }
}

resource "aws_elastic_beanstalk_application_version" "default" {
  name        = "${var.project_name}-application-version"
  application = aws_elastic_beanstalk_application.beanstalk_app.name
  description = "Application version created with Terraform"
  bucket      = aws_s3_bucket.beanstalk-s3-bucket.id
  key         = aws_s3_object.docker_run.id
  tags        = {}
}

resource "aws_elastic_beanstalk_application" "beanstalk_app" {
  name        = var.project_name
  description = "Beanstalk Application for ${var.project_name}"

  appversion_lifecycle {
    service_role          = aws_iam_role.beanstalk_service_role.arn
    max_count             = 3
    delete_source_from_s3 = true
  }
}

resource "aws_iam_role" "beanstalk_service_role" {
  name               = "${var.project_name}-beanstalk-service-role"
  assume_role_policy = data.aws_iam_policy_document.beanstalk_service_role_assume_role_policy.json
  managed_policy_arns = [
    "arn:aws:iam::aws:policy/AWSElasticBeanstalkManagedUpdatesCustomerRolePolicy",
    "arn:aws:iam::aws:policy/service-role/AWSElasticBeanstalkEnhancedHealth",
    # https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/iam-instanceprofile.html
    "arn:aws:iam::aws:policy/AWSElasticBeanstalkWebTier",
    "arn:aws:iam::aws:policy/AWSElasticBeanstalkWorkerTier",
    "arn:aws:iam::aws:policy/AWSElasticBeanstalkMulticontainerDocker",
    "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess",
    aws_iam_policy.additional_service_roles.arn,
  ]
}

resource "aws_iam_instance_profile" "beanstalk_instance_profile" {
  name = "${var.project_name}-beanstalk-instance-profile"
  role = aws_iam_role.beanstalk_service_role.name
  tags = {}
}

data "aws_iam_policy_document" "additional_service_roles" {
  statement {
    sid = "EcrAuthorization"
    actions = [
      "ecr:GetAuthorizationToken",
      "ecr:BatchGetImage",
      "ecr:GetDownloadUrlForLayer",
    ]
    resources = [
      "*",
    ]
  }
}

resource "aws_iam_policy" "additional_service_roles" {
  name   = "${var.project_name}-additional-service-role"
  policy = data.aws_iam_policy_document.additional_service_roles.json
}

data "aws_iam_policy_document" "beanstalk_service_role_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type = "Service"
      identifiers = [
        "elasticbeanstalk.amazonaws.com",
        "ec2.amazonaws.com",
      ]
    }
  }
}

# https://github.com/becketalservices/public-cloudformation-template/blob/master/elasticbeanstalk/config/Dockerrun.aws.json
data "template_file" "docker_run" {
  template = file("${path.module}/Dockerrun.aws.json")
  vars = {
    ECR_URI        = var.ecr_repository.repository_url
    VERSION        = var.ecr_tag
    CONTAINER_PORT = "3000"
  }
}

resource "aws_s3_bucket" "beanstalk-s3-bucket" {
  bucket = var.project_name
  force_destroy = var.force_destroy_s3_buckets
}

resource "aws_s3_object" "docker_run" {
  bucket  = aws_s3_bucket.beanstalk-s3-bucket.id
  key     = "Dockerrun.aws.json"
  content = data.template_file.docker_run.rendered
}

resource "aws_security_group" "ec2_instances" {
  name        = "${var.project_name}-ec2-auto-launch"
  description = "Security group to be used by auto launched EC2 instances"
  vpc_id      = aws_vpc.vpc.id
}

resource "aws_security_group_rule" "ec2_http_access" {
  type                     = "ingress"
  description              = "HTTP access"
  from_port                = 80
  to_port                  = 80
  protocol                 = "tcp"
  security_group_id        = aws_security_group.ec2_instances.id
  source_security_group_id = aws_security_group.elb.id
}

# https://stackoverflow.com/questions/56917634/amazon-ec2-instance-connect-for-ssh-security-group
# https://docs.aws.amazon.com/general/latest/gr/aws-ip-ranges.html
# https://ip-ranges.amazonaws.com/ip-ranges.json
data "aws_ip_ranges" "ec2_instance_connect" {
  regions  = [var.region]
  services = ["ec2_instance_connect"]
}

resource "aws_security_group_rule" "ssh_access" {
  type              = "ingress"
  description       = "SSH access"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = data.aws_ip_ranges.ec2_instance_connect.cidr_blocks
  prefix_list_ids   = []
  security_group_id = aws_security_group.ec2_instances.id
}

resource "aws_security_group" "elb" {
  name        = "${var.project_name}-elb"
  description = "Security group to be used by the ELB"
  vpc_id      = aws_vpc.vpc.id
}

resource "aws_security_group_rule" "elb_http_access" {
  type              = "ingress"
  description       = "HTTP access"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.elb.id
}