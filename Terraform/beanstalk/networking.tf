locals {
  main_zone_id = var.parent_domain_name == "" || var.parent_domain_name == var.domain_name ? data.aws_route53_zone.existing_route53_zone[0].zone_id : aws_route53_zone.route53_zone[0].zone_id
}

resource "aws_vpc" "vpc" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "${var.project_name}-beanstalk"
  }
  tags_all = {
    Name = "${var.project_name}-beanstalk"
  }
}

resource "aws_internet_gateway" "internet_gateway" {
  vpc_id = aws_vpc.vpc.id
  tags = {
    Name = "${var.project_name}-beanstalk"
  }
}

resource "aws_subnet" "public_subnet_a" {
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "${var.region}a"
  tags = {
    Name = "${var.project_name}-beanstalk-public-a"
  }
}

resource "aws_subnet" "public_subnet_b" {
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = "10.0.2.0/24"
  availability_zone = "${var.region}b"
  tags = {
    Name = "${var.project_name}-beanstalk-public-b"
  }
}

resource "aws_route_table" "public_route_table_a" {
  vpc_id = aws_vpc.vpc.id
  tags = {
    Name = "${var.project_name}-beanstalk-public-a"
  }
}

resource "aws_route_table" "public_route_table_b" {
  vpc_id = aws_vpc.vpc.id
  tags = {
    Name = "${var.project_name}-beanstalk-public-b"
  }
}

resource "aws_route" "internet_gateway" {
  route_table_id         = aws_route_table.public_route_table_a.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.internet_gateway.id
}

resource "aws_route" "internet_gateway_b" {
  route_table_id         = aws_route_table.public_route_table_b.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.internet_gateway.id
}

resource "aws_route_table_association" "public_association_a" {
  subnet_id      = aws_subnet.public_subnet_a.id
  route_table_id = aws_route_table.public_route_table_a.id
}

resource "aws_route_table_association" "public_association_b" {
  subnet_id      = aws_subnet.public_subnet_b.id
  route_table_id = aws_route_table.public_route_table_b.id
}

resource "aws_acm_certificate" "ssl_cert" {
  domain_name       = var.domain_name
  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

data "aws_route53_zone" "existing_route53_zone" {
  count = var.parent_domain_name == "" || var.parent_domain_name == var.domain_name ? 1 : 0
  name  = var.domain_name
}

resource "aws_route53_zone" "route53_zone" {
  count = var.parent_domain_name == "" || var.parent_domain_name == var.domain_name ? 0 : 1
  name  = var.domain_name
}

# This is used for situations where the domain_name is a sub domain
data "aws_route53_zone" "main_zone" {
  count = var.parent_domain_name == "" || var.parent_domain_name == var.domain_name ? 0 : 1
  name  = var.parent_domain_name
}

# This is used for situations where the domain_name is a sub domain
resource "aws_route53_record" "domain_namespace" {
  count   = var.parent_domain_name == "" || var.parent_domain_name == var.domain_name ? 0 : 1
  zone_id = data.aws_route53_zone.main_zone[0].zone_id
  name    = var.domain_name
  type    = "NS"
  ttl     = "30"
  records = aws_route53_zone.route53_zone[0].name_servers
}

resource "aws_route53_record" "domain" {
  for_each = {
    for dvo in aws_acm_certificate.ssl_cert.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = local.main_zone_id
}

data "aws_elastic_beanstalk_hosted_zone" "beanstalk_zone" {}

# https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/routing-to-beanstalk-environment.html
# https://github.com/emreoztoprak/terraform-aws-elasticbeanstalk/blob/master/main.tf
resource "aws_route53_record" "ssl_traffic" {
  zone_id = local.main_zone_id
  name    = var.domain_name
  type    = "A"

  alias {
    name                   = aws_elastic_beanstalk_environment.test.cname
    zone_id                = data.aws_elastic_beanstalk_hosted_zone.beanstalk_zone.id
    evaluate_target_health = false
  }
}