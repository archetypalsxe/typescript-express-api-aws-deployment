output "site_url" {
  value = "https://${var.domain_name}/graphql"
  description = "The URL that the site can be accessed from"
}

output "health_url" {
  value = "https://${var.domain_name}/.well-known/apollo/server-health"
  description = "The URL to be hit for a health check"
}