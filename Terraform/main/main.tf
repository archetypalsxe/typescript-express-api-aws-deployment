# https://github.com/hashicorp/terraform/issues/13022#issuecomment-1160613849
terraform {
  backend "s3" {
    # Cannot use variables
    bucket = "terraform-state-bucket-typescript-express"
    key    = "tfstate/terraform.tfstate"
    # Cannot use variables
    region = "us-east-1"
    # Cannot use variables
    dynamodb_table = "terraform-lock-table-typescript-express"
  }
}

provider "aws" {
  profile = var.aws_profile
  region  = var.region
  default_tags {
    tags = {
      ProjectName = var.project_name
      Owner = "terraform"
    }
  }
}

data "aws_ecr_repository" "ecr" {
  name = var.project_name
}

module "elastic_beanstalk" {
  source             = "../beanstalk"
  region             = var.region
  project_name       = var.project_name
  ecr_tag            = var.ecr_tag
  ecr_repository     = data.aws_ecr_repository.ecr
  count              = var.deploy_elastic_beanstalk ? 1 : 0
  domain_name        = var.beanstalk_domain_name
  parent_domain_name = var.beanstalk_parent_domain_name
  force_destroy_s3_buckets = var.force_destroy_s3_buckets
}

module "fargate" {
  source             = "../fargate"
  region             = var.region
  project_name       = var.project_name
  ecr_tag            = var.ecr_tag
  ecr_repository     = data.aws_ecr_repository.ecr
  count              = var.deploy_fargate ? 1 : 0
  domain_name        = var.fargate_domain_name
  parent_domain_name = var.fargate_parent_domain_name
}
