output "beanstalk_url" {
  value = var.deploy_elastic_beanstalk ? module.elastic_beanstalk[0].site_url : "Not deployed"
  description = "The URL that the site deployed on Beanstalk can be accessed from"
}

output "beanstalk_health_url" {
  value = var.deploy_elastic_beanstalk ? module.elastic_beanstalk[0].health_url : "Not deployed"
  description = "The URL to use for a health check for the site deployed on Beanstalk"
}

output "fargate_url" {
  value = var.deploy_fargate ? module.fargate[0].site_url : "Not deployed"
  description = "The URL that the site deployed on Fargate can be accessed from"
}

output "fargate_health_url" {
  value = var.deploy_fargate ? module.fargate[0].health_url : "Not deployed"
  description = "The URL to use for a health check for the site deployed on Fargate"
}