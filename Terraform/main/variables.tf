variable "region" {
  description = "Region of the AWS VPC"
  type        = string
}

variable "aws_profile" {
  description = "The AWS profile we should be using to interact with Terraform"
  type        = string
}

variable "project_name" {
  description = "The name of the project that we are building"
  default     = "typescript-express"
  type        = string
}

variable "deploy_elastic_beanstalk" {
  description = "Whether or not to deploy an Elastic Beanstalk environment"
  type        = bool
}

variable "deploy_fargate" {
  description = "Whether or not to deploy an ECS with Fargate environment"
  type        = bool
}

variable "data_dog_external_id" {
  description = "The external ID that is unique to your Data Dog account"
  type        = string
  default     = ""
}

variable "data_dog_api_key" {
  description = "The API Key generated through Data Dog"
  type        = string
  default     = ""
}

variable "data_dog_app_key" {
  description = "The application key generated through Data Dog"
  type        = string
  default     = ""
}

variable "ecr_tag" {
  description = "Which tag we should be looking for within ECR to use"
  default     = "latest"
  type        = string
}

variable "fargate_domain_name" {
  description = "The domain name to create an SSL certificate for with Fargate"
  type        = string
}

variable "fargate_parent_domain_name" {
  description = "If the domain_name is a sub domain, what the parent domain is with Fargate"
  type        = string
  default     = ""
}

variable "beanstalk_domain_name" {
  description = "The domain name to create an SSL certificate for with Beanstalk"
  type        = string
}

variable "beanstalk_parent_domain_name" {
  description = "If the domain_name is a sub domain, what the parent domain is with Beanstalk"
  type        = string
  default     = ""
}

variable "force_destroy_s3_buckets" {
  description = "Whether or not S3 buckets should be able to be deleted with files in them by default"
  default     = false
  type        = bool
}
