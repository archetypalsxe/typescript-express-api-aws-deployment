# https://www.howtoforge.com/how-to-manage-terraform-state-in-aws-s3-bucket/
# https://cloudtruth.com/blog/terraform-remote-state-aws-backend/
# https://www.terraform.io/language/settings/backends/s3
terraform {
  backend "local" {
    # This can't be the default or else Terratest tries to migrate it
    path = "terraform-state.tfstate"
  }
}

provider "aws" {
  profile = var.aws_profile
  region  = var.region
}

resource "aws_s3_bucket" "state_bucket" {
  bucket = "terraform-state-bucket-${var.project_name}"
  force_destroy = var.force_destroy_s3_buckets
}

resource "aws_dynamodb_table" "terraform_locks" {
  name         = "terraform-lock-table-${var.project_name}"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
}

# We really want to deploy the ECR repository first as all other builds will
# fail until this is present and an image is pushed to it
resource "aws_ecr_repository" "ecr" {
  name                 = var.project_name
  image_tag_mutability = "MUTABLE"
  image_scanning_configuration {
    scan_on_push = true
  }
  force_delete = var.force_destroy_ecr_repository
}